import { BrowserRouter, Route, Router, Routes } from 'react-router-dom';
import './App.css';
import Spinner from './components/Spinner/Spinner';
import { movieRoutes } from './routes/movieRoutes';

function App() {
  return (
    <div>
      {/* <Spinner /> */}

      <BrowserRouter>
        <Routes>
          {movieRoutes.map(({ url, component }) => {
            return <Route path={url} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
