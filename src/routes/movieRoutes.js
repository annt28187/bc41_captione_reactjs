import DetailPage from '../Pages/DetailPage/DetailPage';
import HomePage from '../Pages/HomePage/HomePage';
import SignInPage from '../Pages/SignInPage/SignInPage';
import SignUpPage from '../Pages/SignUpPage/SignUpPage';
import UnderConstructionPage from '../Pages/UnderConstructionPage/UnderConstructionPage';
import NotFoundPage from './../Pages/NotFoundPage/NotFoundPage';

export const movieRoutes = [
  {
    url: '/',
    component: <HomePage />,
  },
  {
    url: '/sign-in',
    component: <SignInPage />,
  },
  {
    url: '/sign-up',
    component: <SignUpPage />,
  },
  {
    url: '/detail',
    component: <DetailPage />,
  },
  {
    url: '/news',
    component: <UnderConstructionPage />,
  },
  {
    url: '/showtimes',
    component: <UnderConstructionPage />,
  },
  {
    url: '/theater-cluster',
    component: <UnderConstructionPage />,
  },
  {
    url: '/application',
    component: <UnderConstructionPage />,
  },
  {
    url: '*',
    component: <NotFoundPage />,
  },
];
