import DetailPage from '../pages/DetailPage/DetailPage';
import HomePage from '../pages/HomePage/HomePage';
import SignInPage from '../pages/SignInPage/SignInPage';
import SignUpPage from '../pages/SignUpPage/SignUpPage';

export const movieRoutes = [
  {
    url: '/',
    component: <HomePage />,
  },
  {
    url: '/sign-in',
    component: <SignInPage />,
  },
  {
    url: '/sign-up',
    component: <SignUpPage />,
  },
  {
    url: '/detail',
    component: <DetailPage />,
  },
];
