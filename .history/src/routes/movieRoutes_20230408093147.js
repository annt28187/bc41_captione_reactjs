import DetailPage from '../Pages/DetailPage/DetailPage';
import HomePage from '../Pages/HomePage/HomePage';
import SignInPage from '../Pages/SignInPage/SignInPage';
import SignUpPage from '../Pages/SignUpPage/SignUpPage';
import NotFoundPage from './../Pages/NotFoundPage/NotFoundPage';

export const movieRoutes = [
  {
    url: '/',
    component: <HomePage />,
  },
  {
    url: '/sign-in',
    component: <SignInPage />,
  },
  {
    url: '/sign-up',
    component: <SignUpPage />,
  },
  {
    url: '/detail',
    component: <DetailPage />,
  },
  {
    url: '*',
    component: <NotFoundPage />,
  },
];
