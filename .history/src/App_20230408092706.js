import { BrowserRouter, Route, Router } from 'react-router-dom';
import './App.css';
import Spinner from './components/Spinner/Spinner';
import { movieRoutes } from './routes/movieRoutes';

function App() {
  return (
    <div>
      <Spinner />

      <BrowserRouter>
        <Router>
          {movieRoutes.map(({ url, component }) => {
            return <Route path={url} element={component} />;
          })}
        </Router>
      </BrowserRouter>
    </div>
  );
}

export default App;
