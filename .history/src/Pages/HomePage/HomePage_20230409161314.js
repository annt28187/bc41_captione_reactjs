import React from 'react';
import Header from '../../components/Header/Header';
import HomeCarousel from '../../components/HomeCarousel/HomeCarousel';

export default function HomePage() {
  return (
    <div>
      <Header />
      <HomeCarousel />
      HomePage
    </div>
  );
}
