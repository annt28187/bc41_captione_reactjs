import React from 'react';
import Header from '../../components/Header/Header';
import Carousel from './../../components/Carousel/Carousel';

export default function HomePage() {
  return (
    <div>
      <Header />
      <Carousel />
      HomePage
    </div>
  );
}
