import React from 'react';
import Lottie from 'lottie-react';
import bg_uc from '../../assets/animates/under-construction.json';

export default function UnderConstructionPage() {
  return (
    <div className="w-screen h-screen">
      <Lottie animationData={bg_uc} loop={true} />
    </div>
  );
}
