import React from 'react';
import Lottie from 'lottie-react';
import bg_uc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function UnderConstructionPage() {
  return (
    <div className="w-screen h-screen p-5">
      <div className="flex items-center justify-center ">
        <NavLink to="/">
          <button className="border-solid border-2 border-sky-500 px-8 py-3 rounded-md">
            Trang Chủ
          </button>
        </NavLink>
      </div>
      <Lottie animationData={bg_uc} loop={true} />
    </div>
  );
}
