import React from 'react';
import Lottie from 'lottie-react';
import bg_uc from '../../assets/animates/under-construction.json';
import { NavLink } from 'react-router-dom';

export default function UnderConstructionPage() {
  return (
    <div className="w-screen h-screen">
      <NavLink to="/" className="flex">
        <button className="algin-center px-8 py-3 rounded">Trang Chủ</button>
      </NavLink>
      <Lottie animationData={bg_uc} loop={true} />
    </div>
  );
}
