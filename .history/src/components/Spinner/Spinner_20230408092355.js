import React from 'react';
import { useSelector } from 'react-redux';
import { HashLoader } from 'react-spinners';

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div
      style={{ background: '#ffe8d6' }}
      className="fixed w-screen h-screen top-0 left-0 flex justify-center items-center z-50 "
    >
      <HashLoader size={48} speedMultiplier={2} color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
