import React from 'react';
import { Carousel } from 'antd';
const contentStyle = {
  margin: 0,
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

export default function HomeCarousel() {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  return (
    <Carousel effect="fade">
      <div>
        <div style={contentStyle}>
          <img src="https://picsum.photo/1000" className="w-full" alt="123" />
        </div>
      </div>
      <div>
        <div style={contentStyle}>
          <img src="https://picsum.photo/1000" className="w-full" alt="123" />
        </div>
      </div>
      <div>
        <div style={contentStyle}>
          <img src="https://picsum.photo/1000" className="w-full" alt="123" />
        </div>
      </div>
      <div>
        <div style={contentStyle}>
          <img src="https://picsum.photo/1000" className="w-full" alt="123" />
        </div>
      </div>
    </Carousel>
  );
}
