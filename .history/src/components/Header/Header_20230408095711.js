import React from 'react';
import { Desktop } from '../../layout/Responsive';
import DestopHeader from './DestopHeader';

export default function Header() {
  return (
    <div>
      <Desktop>
        <DestopHeader />
      </Desktop>
    </div>
  );
}
