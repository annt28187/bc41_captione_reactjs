import React from 'react';
import { Desktop, Mobile, Tablet } from '../../layout/Responsive';
import DestopHeader from './DestopHeader';
import TabletHeader from './TabletHeader';
import MobileHeader from './MobileHeader';

export default function Header() {
  return (
    <div>
      <Desktop>
        <DestopHeader />
      </Desktop>
      <Tablet>
        <TabletHeader />
      </Tablet>
      <Mobile>
        <MobileHeader />
      </Mobile>
    </div>
  );
}
